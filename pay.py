
hours = input('Enter Hours: ')
try:
    float(hours)
except:
    print('Error, please enter numbers only.')
    exit()
rate = input('Enter Rate: ')
try:
    float(rate)
except:
    print('Error, please enter numbers only.')
    exit()


if float(hours) > 40.0:
    OT = (float(hours) - 40.0) * (float(rate) * 1.5)
    pay = (40 * float(rate)) + float(OT)
else:
    pay = float(hours) * float(rate)
print('Pay =', pay)

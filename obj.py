import random


class PartyAnimal:
    x = 0
    name = ""

    def __init__(self, z):
        self.name = z
        if self.x > 49:
            print(self.name, 'constructed')

    def party(self):
        # print(self.name, 'party count', self.x)
        self.x = self.x + 1
        if self.x > 49:
            print(self.name, 'party count', self.x)


s = PartyAnimal("Sally")


j = PartyAnimal('Johnny')


for i in range(100):
    ct = random.randint(0, 55)
    if ct % 2 == 0:
        s.party()
        if i > 85:
            print(i)
    else:
        j.party()
        if i > 85:
            print(i)


def to_far(inp):
    try:
        cel = float(inp)
        fahr = (((cel * 9.0) / 5.0) + 32.0)
        print(str(int(fahr)), 'degrees Fahrenheit')
    except:
        unk_prob()


def to_cel(inp):
    try:
        fahr = float(inp)
        cel = (((fahr - 32.0) * 5.0) / 9.0)
        print(str(int(cel)), 'degrees Celsius')
    except:
        unk_prob()


def num_only():
    print("\nNumbers only; please try again!\n")


def unk_prob():
    print("\nSorry, I ran into a problem that I can't resolve.\n")


inp = input("Temperature: ")
try:
    float(inp)
except:
    num_only()
    exit()

scale = input("Fahrenheit (F) or Celsius (C)? ")
if scale in ("F", "f"):
    to_cel(inp)
elif scale in ("C", "c"):
    to_far(inp)
else:
    print("\n(F)ahrenheit or (C)elsius are the only options. Try again.\n")

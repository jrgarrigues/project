
def to_far(inp):
    try:
        cel = float(inp)
        fahr = (((cel * 9.0) / 5.0) + 32.0)
        print(str(int(fahr)), 'degrees Fahrenheit')
    except:
        print("Numbers only please!")


def to_cel(inp):
    try:
        fahr = float(inp)
        cel = (((fahr - 32.0) * 5.0) / 9.0)
        print(str(int(cel)), 'degrees Celsius')
    except:
        print("Numbers only please!")


inp = input("Temperature: ")
try:
    float(inp)
except:
    print("Numbers only; please try again!")
    exit()

scale = input("Fahrenheit (F) or Celsius (C)? ")
if scale in ("F", "f"):
    to_cel(inp)
elif scale in ("C", "c"):
    to_far(inp)
else:
    print("(F)ahrenheit or (C)elsius are the only options. Try again.")


# if x == y:
# 	print('x and y are equal')
# else:
# 	if x < y:
# 		print('x is less than y')
# 	else:
# 		print('x is greater than y')


score = input('Enter a score (0.0 to 1.0): ')
try:
    float(score)
except:
    print("\nNumbers only; please try again!\n")
    exit()


if float(score) > 1.0:
    print('Error, score is too high.')
elif float(score) >= 0.9:
    print('A')
elif float(score) >= 0.8:
    print('B')
elif float(score) >= 0.7:
    print('C')
elif float(score) >= 0.6:
    print('D')
elif float(score) >= 0.0:
    print('F')
if float(score) < 0.0:
    print('Error, score is too low.')


